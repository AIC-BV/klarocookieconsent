# Klaro Cookie Consent for WinterCMS

WinterCMS plugin that integrates the cookie manager from https://klaro.kiprotect.com/.
Read more about the implementation in your website here: https://heyklaro.com/docs/getting-started
